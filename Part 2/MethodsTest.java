public class MethodsTest{
	
	public static void main(String[] args){
		
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(x, 3.5);
		int z = methodNoInputReturnInt();
		System.out.println("Print the no input return int method value saved as z: " + z);
		double addRoot = sumSquareRoot(6, 3);
		System.out.println(addRoot);
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn(){
		
		System.out.println("I'm a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int y){
	
		System.out.println("Inside the method one input no return " + y);
	}
	
	public static void methodTwoInputNoReturn(int y, double x){
		
		System.out.println("Inside the method two input no return, int: " + y + " double: " + x);
	}
	
	public static int methodNoInputReturnInt(){
		return 6;
		
	}
	
	public static double sumSquareRoot(int value1, int value2){
		
		double valueToRoot = value1+value2;
		return Math.sqrt(valueToRoot);
	}
}
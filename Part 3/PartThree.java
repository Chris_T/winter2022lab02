import java.util.Scanner;

public class PartThree{
	
	public static void main(String[] args){
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter square length");
		double squareLength = scan.nextDouble();
		System.out.println("Please enter rectangle length");
		double rectangleLength = scan.nextDouble();
		System.out.println("Please enter rectangle width");
		double rectangleWidth = scan.nextDouble();
		
		AreaComputations ac = new AreaComputations();
		
		System.out.println("The area of the square is: " + AreaComputations.areaSquare(squareLength));
		System.out.println("The area of the rectangle is: " + ac.areaRectangle(rectangleLength, rectangleWidth));
	}
}
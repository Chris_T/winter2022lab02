public class AreaComputations{
	
	public static double areaSquare(double length){
		
		return length*length;
	}
	
	public double areaRectangle(double length, double width){
		
		return length*width;
	}
}